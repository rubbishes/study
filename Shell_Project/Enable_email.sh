
#- - - - - - - - -- --Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2022-07-20
#FileName：                     Enable_email.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2022All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#!/bin/bash
# 此脚本为邮箱账号开启双重认证
# 定义传参
# query=$1
# 获取token API 接口
url="https://api.exmail.qq.com/cgi-bin"
# corpid
id="wmdd7e5"
# corpsecret
secrect="0i9jTuae2CAHsI"
# 获取 token
token=`curl -s "$url/gettoken?corpid=$id&corpsecret=$secrect"| /opt/homebrew/bin/jq -r .access_token`
# echo $token
# 定义一个时间变量
DATE_TEST=$(date +'%F %T')
# 更新用户
updateurl="$url/useroption/update?access_token=$token"
while read line;do
    update_msg='{"userid": "'$line'","option": [{"type":1,"value":"1"}]}'
    echo $update_msg
    curl -s -X POST -d "$update_msg" "$updateurl"
    if [ $? -eq 0 ];then
        echo $line"开启成功"
        curl 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=8c86' \
            -H 'Content-Type: application/json' \
            -d ' {
                    "msgtype": "markdown",
                    "markdown": {
                                    "content": "'"用户: <font color=\\"warning\\">$line<\/font>,开启成功。\n
                                    >时间:<font color=\\"comment\\">$DATE_TEST<\/font>"'"
                                }
                }'
    else
        echo $line"开启失败"
        curl 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=021-4e48-8550-2d6b3f8b8c86' \
            -H 'Content-Type: application/json' \
            -d ' {
                    "msgtype": "markdown",
                    "markdown": {
                                    "content": "'"用户: <font color=\\"warning\\">$line<\/font>,开启失败。\n
                                    >时间:<font color=\\"comment\\">$DATE_TEST<\/font>"'"
                                }
                }'
    fi
done < /Users/weidian/Desktop/email

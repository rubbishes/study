#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2020-12-22
#FileName：                     disk.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2020All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
GREEN="\033[1;32m"
RED="\033[1;31m"
END="\033[0m"
BLUE="\033[1;34m"
echo -e "$GREEN---------------显示硬盘最大利用率-----------------$END"
echo -e "		$(df -h | head -1 |tr -s " " | cut -d " " -f1,5)"
echo -e "		$(df -h | tail -n+2 | tr -s " " | sort -nrt" " -k5| cut -d " " -f1,5 |head -1 )"

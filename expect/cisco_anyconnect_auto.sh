
#- - - - - - - - -- --Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2022-11-14
#FileName：                     cisco_anyconnect_auto.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2022All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
# 此脚本默认需要传三个参数分别是账号,密码,动态密钥,如果需要实现完全自动化的 则需要分别将三个变量参数更换成你的账号密码即可.
#!/bin/bash
# #!/usr/bin/expect
# set host [lindex $argv 0]
# set username [lindex $argv 1]
# set password [lindex $argv 2]
# set OTP_CODE [lindex $argv 3]
# spawn /opt/cisco/anyconnect/bin/vpn connect $host
# expect "Username:" {
#         send $username
#         send "\r"
# }
# expect "Password:" {
#         send $password$OTP_CODE
#         send "\r"
# }
# interact
host="vpn.local.com"#需要更换一下这里的地址.
username=$1
password=$2
OTP_CODE=$(oathtool --totp -b "$3")
echo ${host} "${username}" "${password}${OTP_CODE}"
expect <<EOF
spawn /opt/cisco/anyconnect/bin/vpn connect $host
expect "Username:" {
        send $username
        send "\r"
}
expect "Password:" {
        send "$password$OTP_CODE"
        send "\r"
}
expect eof
# expect 脚本#
# expect脚本必须以interact或expect eof结束，执行自动化任务通常expect eof就够了
# expect eof是在等待结束标志。由spawn启动的命令在结束时会产生一个eof标记，expect eof即在等待这个标记
EOF

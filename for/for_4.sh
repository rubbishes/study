#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-02
#FileName：                     for_4.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#9X9乘法表
#\t是tab\c是空格
for i in {1..9};do
	for j in $(seq $i);do
		echo  -e  "${i}x${j}=$((j*i))\t\c"
	done
	echo ""
done

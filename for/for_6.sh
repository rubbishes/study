#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-05
#FileName：                     for_6.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#生成一个等边三角形
read -p "请输入要生成的列数：" LINE
for i in $(seq "$LINE");do
    for ((k=0;k<="$LINE"-i;k++));do
        echo -e " \c"
    done
    sum=$(echo "2*$i-1"|bc)
    for j in $(seq "$sum");do
        echo -e "*\c"
    done
    echo
done
#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-01-29
#FileName：                     ip_check.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#该脚本用于采集某个C类网络存活主机的MAC地址与在使用IP地址
#ETH=$(ifconfig | grep eth | awk '{print $1}')
read -p "请输入你的网卡名:" ETH
echo "开始检测存活主机......."
read -p "请输入你的网段:" NET

for ip in {1..254}
do
{
#如果主机存活，会返回reply from的信息；
arping -c 2 -w 1 "$NET""$ip" -I "$ETH" | grep "reply from" > /dev/null
        if [ $? -eq 0 ]
        then
                MAC=$(arp -n | grep "$NET$ip " | awk '{print $3}')
                echo "$NET$ip 存活 " >> ip.txt
                echo "$NET$ip 存活 ,对应的MAC地址为：$MAC" >> arpmac2.txt
        fi
} &
# 中括号加& 是后台多线程执行，速度优化
done
wait
echo "结束,存活IP在ip.txt文件中 "

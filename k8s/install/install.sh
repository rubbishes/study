#!/bin/bash
# step 1: 安装必要的一些系统工具
sudo yum install -y yum-utils device-mapper-persistent-data lvm2 vim chrony
# Step 2: 添加软件源信息
sudo yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
# Step 3
sudo sed -i 's+download.docker.com+mirrors.aliyun.com/docker-ce+' /etc/yum.repos.d/docker-ce.repo
# Step 4: 更新并安装Docker-CE
sudo yum makecache fast
sudo yum -y install docker-ce
# Step 4: 开启Docker服务
sudo systemctl start docker
sudo systemctl enable docker
# Step 5:关闭swap
swapoff -a && sed -ni.bak '/swap/s/^/#/' /etc/fstab && mount -a && echo "swap关闭成功"
# Step 6:开启chrony服务
systemctl enbale chronyd && systemctl restart chronyd && echo "chrony开启成功" || echo "chrony开启失败"
# Step 7:关闭防火墙并重启docker 服务
systemctl stop firewalld && systemctl disable firewalld && systemctl restart docker && echo "防火墙已关闭成功" || echo "错误"
#关闭sellinux
setenforce 0
sed -i 's/enforcing/disabled/' /etc/selinux/config

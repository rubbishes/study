#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-01-21
#FileName：                     kill.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#这是一个查找你想要查找服务的进程
if [  -v "$1" ];then
	echo "请输入正确服务名"
else
#pid是一个查找你输变量名进程，并筛选掉文件名临时进程号与grep进程号。
pid=(" $(ps -ef | grep "$1" | grep -v "$0" | grep -v grep | tr -s " " | cut -d " " -f2) ")
		echo "${pid[@]}"
		echo "\$0的进程号：$0" 
#	done
fi
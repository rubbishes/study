
#- - - - - - - - -- --Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2022-07-27
#FileName：                     ggg.py
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2022All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
import ctypes.wintypes
import sys,pendulum
sys.path.append(".")
import urllib.request
import json
import xlwt
import pandas as pd
import requests
import datetime
# 定义date_week()函数，作用提取当前这周的开始与结束日期
def date_week():
    # 获取当前时间
    dt = pendulum.now()
    # 获取当前日期这周的开始时间，并赋值temp_start
    temp_start = dt.start_of('week')
    # 转换获取这周的开始时间为年月日格式，并赋值给start
    start = temp_start.strftime('%Y-%m-%d')
    # print("本周开始时间",start)
    # 同理获取这周的结束时间，并赋值给temp_end
    temp_end = dt.end_of('week')
    # 同理转换获取这周的开始时间为年月日格式，并赋值给end
    end = temp_end.strftime('%Y-%m-%d')
    # 将start,end 两个变量返回，以便函数外能接收，因为是两个变量，所以使用了列表的方式。
    return [start, end]
    # print("本周结束时间",end)
def date_range(beginDate, endDate):
    dates = []
    dt = datetime.datetime.strptime(beginDate, "%Y-%m-%d")
    date = beginDate[:]
    while date <= endDate:
        dates.append(date)
        dt = dt + datetime.timedelta(1)
        date = dt.strftime("%Y-%m-%d")
    return dates
# 定义一个获取wdren_token 的函数并且返回值
def get_ticket():
    username = "zhangji"
    password = "Fuck38!!"
    r = requests.post("https://wdren.vdian.net/sso/wdrlogin?sdk_support=wdren.vdian.net",
                      data={"version": "j.1.3.2", "username": username, "password": password})
    wdren_token = r.json().get("result").split("&")[0]
    return wdren_token
#    print (wdren_token)
urllib2 = urllib.request
# 创建一个获取 接口 json 数值的函数
def get_request(url):
    wdren_token = get_ticket()
    try:
        req = urllib2.Request(url)
        req.add_header('cookie','wdr-ticket=' + wdren_token)
        opener = urllib2.urlopen(req)
        content = json.loads(opener.read())
    except Exception as e:
        #print e
        content = None
    return content
# 定义接口url
url = "http://dep.vdian.net/api/issue/requirementRelatedTasks?issueId=103642&sort=planStarttime&order=asc&currentUserName=zhangji&currentUserDisplayName=%E5%BC%A0%E4%BD%B6&isAdmin=false"
# 获取json数据
resp = get_request(url)
# 定义一个空列表
list1 = []
#循环读取json数值
for data in resp["data"]:
    creatorDisplayName = data["creatorDisplayName"]
    if creatorDisplayName == "张佶":
        a = data["name"]
        b = data["creatorDisplayName"]
        dataCreated = data["dateCreated"]
        # 将str 类型值 转换成 datetime 格式
        str_time = datetime.datetime.strptime(dataCreated,"%Y-%m-%d %H:%M:%S")
        # 再次将的到的datetime 格式的值 用指定格式显示
        c_temp = str_time.strftime("%Y-%m-%d")
        # 获取时间范围
        date_list = date_range(date_week()[0], date_week()[1])
        # 循环判断 time_c 的值 是否等于 时间范围，如果等于则赋值给c
        for time_c in date_list:
            if c_temp == time_c:
#                print(c_temp)
                c = c_temp
#        print (type(dataCreated))
#        print (type(c))
                end = {"处理人":b,"问题":a,"创建日期":c}
                # 将end 中的列表添加到list1
                list1.append(end)
# 将list1列表的数据转换成 表格的数据结构
data1 = pd.DataFrame(list1)
# 将data1的值写入当前目录下 temp.xlsx 文件中，并且不生成索引
data1.to_excel("temp.xlsx",index=False)
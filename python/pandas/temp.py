# coding:utf-8
"""
Name:get_wdren_ticket.py
Author:guodong
Time:2021-07-05 3:29 PM
Des:

"""

import requests


def get_ticket():
    username = "zhangji"
    password = ""
    r = requests.post("https://wdren.vdian.net/sso/wdrlogin?sdk_support=wdren.vdian.net",
                      data={"version": "j.1.3.2", "username": username, "password": password})
    wdr_ticket = r.json().get("result").split("&")[0]
    print(wdr_ticket)

if __name__ == "__main__":
    get_ticket()

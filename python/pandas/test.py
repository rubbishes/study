
#- - - - - - - - -- --Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2022-07-28
#FileName：                     test.py
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2022All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
import pendulum,datetime
# 传日期 
#dt = pendulum.datetime(2022, 7, 28)
def date_week():
    dt = pendulum.now()
    temp_start = dt.start_of('week')
    start = temp_start.strftime('%Y-%m-%d')
    # print("本周开始时间",start)
    temp_end = dt.end_of('week')
    end = temp_end.strftime('%Y-%m-%d')
    return [start, end]
    # print("本周结束时间",end)
def date_range(beginDate, endDate):
    dates = []
    dt = datetime.datetime.strptime(beginDate, "%Y-%m-%d")
    date = beginDate[:]
    while date <= endDate:
        dates.append(date)
        dt = dt + datetime.timedelta(1)
        date = dt.strftime("%Y-%m-%d")
    return dates
# date_week()
# aaa = date_range(date_week(0,1))
# print("aaa")
date_list = date_range(date_week()[0],date_week()[1])
print(date_list)
print(date_week()[1])
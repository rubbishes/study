#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-01-17
#FileName：                     sed.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#这是一个讲述sed的用法的脚范例
#范例一：打印passwd第三行跟第五行
sed -n '3p;5p' passwd
#范例二：打印passwd第三行
sed -n '3p' passwd
#范例三:使用扩展正则匹配#开头的行
sed -rn '/^#/p' passwd 
#范例四:通过步进方式显示奇数行或者显示偶数行
seq 10 | sed -n '1~2p'
seq 10 | sed -n '2~2p'
#删除包含root行，并将源文件做个备份
sed -i.bak '/root/d' passwd
#删除不包含root行，并无备份。不建议
sed -i '/root/!d' passwd
#多处匹配替换源内容，这里是将httpd.conf配置里的User test，Group daemon替换成其他内容。这里使用了多个参数-i,-e,'c'
sed -i.bak -e  '/^User test/c User httpd' -e '/^Group daemon/cGroup httpd' httpd.conf
#使用扩展正则替
sed -Ei.bak 's/^(SELINUX=).*/\1enforcing/' /etc/selinux/config
#通过正则表达式可以达到在某一位置插入你想要的内容，如下
#指定在1开头的行末尾添加ddd内容
sed -Ei.bak  '/^1/s/^(1.*)$/\1ddd/'  111
#给没#开头的行首加上#
sed -ri.bak 's/(^[^#].*)/#\1/' fstab
#给没#开头的行尾加#，当然也有多种写法。
#这行的意思是；sed 扩展正匹配fstab文件中非#开头的行，然后查找行尾添加#
sed -Ei.bak '/^#/!s/$/#/' fstab
# 搜索替换ssh客户端配置文件
sed -Ei.bak 's/^# +(StrictHostKeyCheckin).*/\1 no/' /etc/ssh/ssh_config

# 测试的时候可先使用-nr 参数看查看是否争取匹配替换，然后在正式写入文件，例子如下
grep 1695707258@qq.com wms-suk-sum.py
        receivers = ['dddddji@thechenfan.com','2295707258@qq.com']
sed -En "s/(^.*)(,'2295707258@qq.com')(])$/\3/p" wms-suk-sum.py 
sed -En "s/(^.*)(,'2295707258@qq.com')(].*)/\1\3/p" wms-suk-sum.py 
        receivers = ['ddddddji@thechenfan.com']
#查看无误后，再替换并写入文件中
sed -Ei.bak "s/(^.*)(,'2295707258@qq.com')(].*)/\1\3/" wms-suk-sum.py 


#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-01-09
#FileName：                     sync_git.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
git add -A
#注释，你也可以理解为备注。这个信息是可以后面追述的时候 你知道你这次提交的时候干了什么
git commit -m "Script backup"
#推送到git服务器
git push origin
#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-05
#FileName：                     while_2.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#while循环99乘法表

i=1
while [ $i -le 9 ];do
    j=1
    while [ $j -le $i ];do
        let k=i*j
        echo -e "${j}x${i}=$k\t\c"
        let j++
    done
    echo
    let i++
done
#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-05
#FileName：                     while_3.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#while循环打印等腰三角形
read -p "请输入三角形的行数: " LINE
i=1
while [ $i -le $LINE ];do
    j=0
    let k=$LINE-$i
    while [ $j -le $k ];do
        echo -e " \c"
        let j++
    done
    g=1
    let c=2*$i-1
    while [ $g -le $c ];do
        echo -e "*\c"
        let g++
    done
    echo
    let i++
done
#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-05
#FileName：                     while_4.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#利用while循环利用变量RANDOM生成10个随机数字，输出这个10数字，并显示其中的最大值和最小值
max=0
min=32767
i=0
while [ $i -le 10 ] 
do
    num=$RANDOM
    echo $num
    if [ $num -gt $max ]
    then 
        max=$num
    elif [ $num -lt $min ]
    then 
        min=$num
    fi
    let i++
done
echo 'The maximum of 10 random numbers is '${max}
echo "The minimum of 10 random numbers is $min"
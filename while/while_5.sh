#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-05
#FileName：                     while_5.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#实现打印国际象棋棋盘
# for i in {1..8};do 
#     for j in {1..8};do
#     let sum=${i}+${j}
#         if [ $[sum%2] -eq 0 ];then
#         echo -ne "\033[46m  \033[0m"
# #  echo -e "\033[字背景颜色;文字颜色m字符串\033[0m"
# #空两格以致打印出正方形的方格
#         else 
#           echo -ne "\033[47m  \033[0m"
#         fi 
#     done
#     echo 
# done
i=1
while [ $i -le 8 ];do
    j=1
    while [ $j -le 8 ];do
        let sum=${i}+${j}
        if [ $[sum%2] -eq 0 ];then
            echo -e "\033[46m  \033[0m\c"
        else
            echo -ne "\033[47m  \033[0m"
#这里echo用到了两种写法，不过最后的目的都是一样的.都是不追加换行。
        fi
    let j++
    done
    echo
    let i++
done
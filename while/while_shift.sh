#!/bin/bash
#-- - - - - - -Script comment information - - - - - - - - - -
#Author：                               拉基
#Email：                        helpdesk81@outlook.com
#Creation time：                2021-02-05
#FileName：                     while_4.sh
#Blog address：                 https://www.cnblogs.com/98record/
#Copyright (C)                  2021All rights reserved
#- - - - - - - - - - - - - end - - - - - - - - - - - - - - - - - -
#用二个以上的数字为参数，显示其中的最大值和最小值
max=$1
min=$2
if [ $# -eq 0  ];then
	echo "请输入参数"
	exit
else
	echo "输入两个以上参数"
	exit
fi
while [ "$1" ] 
do
    if [ $1 -gt $max ]
    then 
        max=$1
    elif [ $1 -lt $min ]
    then 
        min=$1
    fi
    #let i++
	shift 
done
echo 'The maximum of 10 random numbers is '${max}
echo "The minimum of 10 random numbers is $min"
